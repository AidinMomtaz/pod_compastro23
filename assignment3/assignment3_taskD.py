import numpy as np
import fireworks.ic as fic
import fireworks.nbodylib.dynamics as fdyn
import fireworks.nbodylib.integrators as fint
import fireworks.nbodylib.timesteps as fts
from fireworks.particles import Particles
import matplotlib.pyplot as plt

m1 = 1.
m2 = 2.
rp = 0.5
# List of eccentricities
ecclist = [0.5, 0.9, 0.99]
# List of adaptive time-step strategies
tstratlist = [fts.adaptive_timestep_combined, fts.adaptive_timestep_posvel, fts.adaptive_timestep_velacc, fts.adaptive_timestep_posacc]
# Boundaries for time-step
tmin = 1e-5
tmax = 1.
# List of integrators (except Tsunami)
integratorslist = [fint.integrator_euler, fint.integrator_leapfrog, fint.integrator_rk4]
# Setting the force estimator
estimator = fdyn.acceleration_direct_vectorised

# List of labels for each integrator
labels = ['Tsunami', 'Euler', 'Leapfrog', 'Runge-Kutta 4']
# List of line colors for each integrator (except Tsunami)
colors = ['C1', 'C2', 'C3']
# List of line styles for each adaptive time-step strategy
lslist = ['-', '--', ':', '-.']

# Loop over eccentricities
for e in ecclist:
    
    # Generating initial conditions
    partinitial = fic.ic_two_body(m1, m2, rp, e)
    # Estimating the binary period
    a = rp/(1.-e)
    P = np.sqrt(4.*np.pi**2.*a**3./(m1+m2))

    # Estimating position of center of mass
    compos = partinitial.com_pos()

    # Setting figure 1 for orbits
    fig1,((ax1,ax2),(ax3,ax4)) = plt.subplots(2,2,sharex=True,sharey=True)
    ax1.set_title('Tsunami',fontsize=10.)
    ax2.set_title('Euler',fontsize=10.)
    ax3.set_title('Leapfrog',fontsize=10.)
    ax4.set_title('Runge-Kutta 4',fontsize=10.)
    # Setting figure 2 for time vs total energy
    fig2 = plt.figure()
    # Setting figure 3 for time vs relative energy change
    fig3 = plt.figure()
    # Setting figure 4 for time-step vs energy error
    fig4 = plt.figure()

    # Tsunami integration
    t = 0.
    time = [t]     # List of time instants
    part = partinitial.copy()
    # Lists containing positions of the 2 bodies and total energy
    x1 = [part.pos[0,0]]
    y1 = [part.pos[0,1]]
    x2 = [part.pos[1,0]]
    y2 = [part.pos[1,1]]
    Etot = [part.Etot()[0]]
    # Time-step for Tsunami
    if e==0.5: tstep = P/1000.
    if e==0.9: tstep = P/1000.
    if e==0.99: tstep = P/10000.
    # List of effective time-steps used
    tefflist = []
    # Integration loop
    while(t <= 10.*P):
        # Integration
        part,teffective,_,_,_ = fint.integrator_tsunami(part, tstep)
        # Update positions and total energy
        # NOTE that Tsunami returns positions in the frame centered in the center of mass
        # Energy doesn't need to be corrected since Tsunami and ic_two_body work both with v_com=0
        x1.append(part.pos[0,0]+compos[0])
        y1.append(part.pos[0,1]+compos[1])
        x2.append(part.pos[1,0]+compos[0])
        y2.append(part.pos[1,1]+compos[1])
        Etot.append(part.Etot()[0])
        # Update time with effective time-step used
        t += teffective
        time.append(t)
        # Storing effective time-step used
        tefflist.append(teffective)
    plt.figure(fig1)     # Plot of the orbits
    ax1.plot(x1,y1,color='C0',linewidth=0.5,linestyle='-')
    ax1.plot(x2,y2,color='C1',linewidth=0.5,linestyle='-')
    plt.figure(fig2)     # Plot of time vs total energy
    plt.plot(time,Etot,color='C0',linewidth=0.5,linestyle='-')
    plt.figure(fig3)     # Plot of time vs relative energy change
    plt.plot(time,np.abs((Etot-Etot[0])/Etot[0]),color='C0',linewidth=0.5,linestyle='-')
    plt.figure(fig4)     # Plot of timestep vs energy error
    plt.plot(tefflist,np.abs(Etot[1:]-Etot[0]),color='C0',linewidth=0.5,linestyle='-')
    
    # Other integrators, with adaptive time-step
    # Loop over adaptive time-step strategies
    for tstrat in tstratlist:

        # Setting line style for current tstep
        linestyle = lslist[tstratlist.index(tstrat)]

        # Loop over integrators
        for integrator in integratorslist:

            # Setting line color for current integrator
            color = colors[integratorslist.index(integrator)]

            t = 0.
            time = [t]     # List of time instants
            part = partinitial.copy()
            # Lists containing positions of the 2 bodies and total energy
            x1 = [part.pos[0,0]]
            y1 = [part.pos[0,1]]
            x2 = [part.pos[1,0]]
            y2 = [part.pos[1,1]]
            Etot = [part.Etot()[0]]

            # Some adaptive time-step strategies require acceleration
            # Setting initial acceleration for the first time-step estimation
            part.set_acc(estimator(part)[0])

            # List of effective time-steps used
            tefflist = []

            # Integration loop
            while(t <= 10.*P):

                # Estimation of time-step
                tstep = tstrat(part, tmin, tmax)
                # Integration
                part,teffective,_,_,_ = integrator(part, tstep, estimator)
                # Update positions and total energy
                x1.append(part.pos[0,0])
                y1.append(part.pos[0,1])
                x2.append(part.pos[1,0])
                y2.append(part.pos[1,1])
                Etot.append(part.Etot()[0])
                # Update time with effective time-step used
                t += teffective
                time.append(t)
                # Storing effective time-step used
                tefflist.append(teffective)

            plt.figure(fig1)     # Plot of the orbits
            # Setting axes for the first plot
            if integrator is fint.integrator_euler: ax = ax2
            if integrator is fint.integrator_leapfrog: ax = ax3
            if integrator is fint.integrator_rk4: ax = ax4
            ax.plot(x1,y1,color='C0',linewidth=0.5,linestyle=linestyle)
            ax.plot(x2,y2,color='C1',linewidth=0.5,linestyle=linestyle)

            plt.figure(fig2)     # Plot of time vs total energy
            plt.plot(time,Etot,color=color,linewidth=0.5,linestyle=linestyle)

            plt.figure(fig3)     # Plot of time vs relative energy change
            plt.plot(time,np.abs((Etot-Etot[0])/Etot[0]),color=color,linewidth=0.5,linestyle=linestyle)

            plt.figure(fig4)     # Plot of timestep vs energy error
            plt.plot(tefflist,np.abs(Etot[1:]-Etot[0]),color=color,linewidth=0.5,linestyle=linestyle)

    plt.figure(fig1)
    ax3.set_xlabel('x [nbody]')
    ax4.set_xlabel('x [nbody]')
    ax1.set_ylabel('y [nbody]')
    ax3.set_ylabel('y [nbody]')
    fig1.suptitle('Eccentricity '+str(e))
    fig1.legend(['mass 1', 'mass 2'],loc='upper right',bbox_to_anchor=(1.,1.))
    plt.savefig('orbits - ecc '+str(e)+'.pdf')
    plt.close()

    plt.figure(fig2)
    plt.xlabel('time [nbody]')
    plt.ylabel('total energy [nbody]')
    fig2.suptitle('Eccentricity '+str(e))
    plt.legend(labels)
    plt.savefig('totalenergy - ecc '+str(e)+'.pdf')
    plt.close()

    plt.figure(fig3)
    plt.xlabel('time [nbody]')
    plt.ylabel('relative energy change')
    plt.yscale('log')
    fig3.suptitle('Eccentricity '+str(e))
    plt.legend(labels)
    plt.savefig('symplecticity - ecc '+str(e)+'.pdf')
    plt.close()

    plt.figure(fig4)
    plt.xlabel('timestep [nbody]')
    plt.ylabel('energy error $|E-E_0|$ [nbody]')
    plt.xscale('log')
    plt.yscale('log')
    fig4.suptitle('Eccentricity '+str(e))
    plt.legend(labels)
    plt.savefig('performance - ecc '+str(e)+'.pdf')
    plt.close()