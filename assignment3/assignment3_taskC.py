import numpy as np
import fireworks.ic as fic
import fireworks.nbodylib.dynamics as fdyn
import fireworks.nbodylib.integrators as fint
from fireworks.particles import Particles
import matplotlib.pyplot as plt

# Generating initial conditions
m1 = 1.
m2 = 2.
r = 0.5     # Radius of the orbit
e = 0.      # Circular orbit
partinitial = fic.ic_two_body(m1, m2, r, e)

# List of time-steps to use
tsteplist = [0.01, 0.001, 0.0001]
# List of line styles for each time-step
lslist = ['-', '--', ':']

# Estimating the binary period
P = np.sqrt(4.*np.pi**2.*r**3./(m1+m2))

# List of integrators
integratorslist = [fint.integrator_euler, fint.integrator_leapfrog, fint.integrator_rk4, fint.integrator_hermite]
# List of labels for each integrator
labels = ['Euler', 'Leapfrog', 'Runge-Kutta 4', 'Hermite']
# List of line colors for each integrator
colors = ['C0', 'C1', 'C2', 'C3']

# Setting figure 1 for orbits
fig1,((ax1,ax2),(ax3,ax4)) = plt.subplots(2,2,sharex=True,sharey=True)
ax1.set_title('Euler',fontsize=10.)
ax2.set_title('Leapfrog',fontsize=10.)
ax3.set_title('Runge-Kutta 4',fontsize=10.)
ax4.set_title('Hermite',fontsize=10.)
# Setting figure 2 for time vs total energy
fig2 = plt.figure()
# Setting figure 3 for time vs relative energy change
fig3 = plt.figure()
# Setting figure 4 for time-step vs max energy error
fig4 = plt.figure()

# Loop over timesteps
for tstep in tsteplist:

    # Setting line style for current tstep
    linestyle = lslist[tsteplist.index(tstep)]
    
    # Loop over integrators (NOTE that Hermite requires a jerk estimator)
    for integrator in integratorslist:

        # Setting line color for current integrator
        color = colors[integratorslist.index(integrator)]
        
        t = 0.
        time = [t]     # List of time instants
        part = partinitial.copy()
        # Lists containing positions of the 2 bodies and total energy
        x1 = [part.pos[0,0]]
        y1 = [part.pos[0,1]]
        x2 = [part.pos[1,0]]
        y2 = [part.pos[1,1]]
        Etot = [part.Etot()[0]]
        
        if integrator is not fint.integrator_hermite: estimator = fdyn.acceleration_direct_vectorised
        else: estimator = fdyn.acceleration_jerk_direct
            
        # Integration loop
        while(t <= 10.*P):
            part,teffective,_,_,_ = integrator(part, tstep, estimator)
            x1.append(part.pos[0,0])
            y1.append(part.pos[0,1])
            x2.append(part.pos[1,0])
            y2.append(part.pos[1,1])
            Etot.append(part.Etot()[0])
            t += teffective      # Update time with effective time-step used
            time.append(t)

        plt.figure(fig1)     # Plot of the orbits
        # Setting axes for the first plot
        if integrator is fint.integrator_euler: ax = ax1
        if integrator is fint.integrator_leapfrog: ax = ax2
        if integrator is fint.integrator_rk4: ax = ax3
        if integrator is fint.integrator_hermite: ax = ax4
        ax.plot(x1,y1,color='C0',linewidth=0.5,linestyle=linestyle)
        ax.plot(x2,y2,color='C1',linewidth=0.5,linestyle=linestyle)
        
        plt.figure(fig2)     # Plot of time vs total energy
        plt.plot(time,Etot,color=color,linewidth=0.5,linestyle=linestyle)
    
        plt.figure(fig3)     # Plot of time vs relative energy change
        plt.plot(time,np.abs((Etot-Etot[0])/Etot[0]),color=color,linewidth=0.5,linestyle=linestyle)
    
        plt.figure(fig4)     # Plot of timestep vs max energy error
        plt.scatter(teffective,np.max(np.abs(Etot-Etot[0])),color=color,s=2)

plt.figure(fig1)
ax3.set_xlabel('x [nbody]')
ax4.set_xlabel('x [nbody]')
ax1.set_ylabel('y [nbody]')
ax3.set_ylabel('y [nbody]')
fig1.legend(['mass 1', 'mass 2'],loc='upper right',bbox_to_anchor=(1.,1.))
plt.savefig('orbits - circular.pdf')
plt.figure(fig2)
plt.xlabel('time [nbody]')
plt.ylabel('total energy [nbody]')
plt.legend(labels)
plt.savefig('totalenergy - circular.pdf')
plt.figure(fig3)
plt.xlabel('time [nbody]')
plt.ylabel('relative energy change')
plt.yscale('log')
plt.legend(labels)
plt.savefig('symplecticity - circular.pdf')
plt.figure(fig4)
plt.xlabel('timestep [nbody]')
plt.ylabel('maximum energy error [nbody]')
plt.xscale('log')
plt.yscale('log')
plt.legend(labels)
plt.savefig('performance - circular.pdf')