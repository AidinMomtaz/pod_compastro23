"""
====================================================================================================================
Collection of functions to estimate the timestep of the Nbody integrations (:mod:`fireworks.nbodylib.timesteps`)
====================================================================================================================

This module contains functions and utilities to estimate the timestep for the Nbody integrations.
There are no strict requirements for these functions. Obviously  it is important that they return a timestep.
It could be also useful to have as inputs a minimum and maximum timestep


"""
from typing import Optional, Tuple, Callable, Union
import numpy as np
import numpy.typing as npt
from ..particles import Particles

def adaptive_timestep_simple(particles: Particles, tmin: Optional[float] = None, tmax: Optional[float] = None) -> float:
    """
    Very simple adaptive timestep based on the ratio between the position and the velocity of the particles.

    :param particles: that is an instance of the class :class:`~fireworks.particles.Particles`
    :return: estimated timestep
    """

    # Simple idea, use the R/V of the particles to have an estimate of the required timestep
    # Take the minimum among all the particles

    ts = np.nanmin(particles.radius()/particles.vel_mod())

    # Check tmin, tmax
    if tmin is not None: ts=np.max((ts,tmin))
    if tmax is not None: ts=np.min((ts,tmax))

    return ts

def adaptive_timestep_posvel(particles: Particles, tmin: Optional[float] = None, tmax: Optional[float] = None) -> float:
    """
    Very simple adaptive timestep based on the ratio between the position and the velocity of the particles.
    The minimum ratio between all particles is taken as timestep.
    A parameter eta is added to adjust the order of magnitude of the value of the ratio.

    :param particles: that is an instance of the class :class:`~fireworks.particles.Particles`
    :return: estimated timestep
    """

    eta = 0.001
    ts = np.nanmin(particles.radius()/particles.vel_mod())
    ts *= eta

    if tmin is not None: ts = np.max((ts,tmin))
    if tmax is not None: ts = np.min((ts,tmax))

    return ts

def adaptive_timestep_velacc(particles: Particles, tmin: Optional[float] = None, tmax: Optional[float] = None) -> float:
    """
    Very simple adaptive timestep based on the ratio between the velocity and the acceleration of the particles.
    The minimum ratio between all particles is taken as timestep.
    A parameter eta is added to adjust the order of magnitude of the value of the ratio.

    :param particles: that is an instance of the class :class:`~fireworks.particles.Particles`
    :return: estimated timestep
    """

    # Acceleration is required. If None use the method adaptive_timestep_posvel
    if particles.acc is None: return adaptive_timestep_posvel(particles,tmin,tmax)
    
    eta = 0.001
    acc_mod = np.sqrt(np.sum(particles.acc*particles.acc, axis=1))[:,np.newaxis]
    ts = np.nanmin(particles.vel_mod()/acc_mod)
    ts *= eta

    if tmin is not None: ts = np.max((ts,tmin))
    if tmax is not None: ts = np.min((ts,tmax))

    return ts

def adaptive_timestep_posacc(particles: Particles, tmin: Optional[float] = None, tmax: Optional[float] = None) -> float:
    """
    Very simple adaptive timestep based on the ratio between the position and the acceleration of the particles.
    The minimum sqrt ratio between all particles is taken as timestep.
    A parameter eta is added to adjust the order of magnitude of the value of the ratio.

    :param particles: that is an instance of the class :class:`~fireworks.particles.Particles`
    :return: estimated timestep
    """
    
    # Acceleration is required. If None use the method adaptive_timestep_posvel
    if particles.acc is None: return adaptive_timestep_posvel(particles,tmin,tmax)

    eta = 0.001
    acc_mod = np.sqrt(np.sum(particles.acc*particles.acc, axis=1))[:,np.newaxis]
    ts = np.nanmin(np.sqrt(particles.radius()/acc_mod))
    ts *= eta

    if tmin is not None: ts = np.max((ts,tmin))
    if tmax is not None: ts = np.min((ts,tmax))

    return ts

def adaptive_timestep_combined(particles: Particles, tmin: Optional[float] = None, tmax: Optional[float] = None) -> float:
    """
    Very simple adaptive timestep based on the minimum estimate of timestep between those provided
    by the ratios R/V, V/A and sqrt(R/A).

    :param particles: that is an instance of the class :class:`~fireworks.particles.Particles`
    :return: estimated timestep
    """
    
    ts1 = adaptive_timestep_posvel(particles,tmin,tmax)
    ts2 = adaptive_timestep_velacc(particles,tmin,tmax)
    ts3 = adaptive_timestep_posacc(particles,tmin,tmax)
    
    return np.min((ts1,ts2,ts3))