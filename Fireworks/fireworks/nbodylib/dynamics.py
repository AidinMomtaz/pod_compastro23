"""
====================================================================================================================
Collection of functions to estimate the Gravitational forces and accelerations (:mod:`fireworks.nbodylib.dynamics`)
====================================================================================================================

This module contains a collection of functions to estimate acceleration due to
gravitational  forces.

Each method implemented in this module should follow the input-output structure show for the
template function  :func:`~acceleration_estimate_template`:

Every function needs to have two input parameters:

    - particles, that is an instance of the class :class:`~fireworks.particles.Particles`
    - softening, it is the gravitational softening. The parameters need to be included even
        if the function is not using it. Use a default value of 0.

The function needs to return a tuple containing three elements:

    - acc, the acceleration estimated for each particle, it needs to be a Nx3 numpy array,
        this element is mandatory it cannot be 0.
    - jerk, time derivative of the acceleration, it is an optional value, if the method
        does not estimate it, just set this element to None. If it is not None, it has
        to be a Nx3 numpy array.
    - pot, gravitational potential at the position of each particle. it is an optional value, if the method
        does not estimate it, just set this element to None. If it is not None, it has
        to be a Nx1 numpy array.


"""
from typing import Optional, Tuple
import numpy as np
import numpy.typing as npt
from ..particles import Particles

try:
    import pyfalcon
    pyfalcon_load=True
except:
    pyfalcon_load=False

def acceleration_estimate_template(particles: Particles, softening: float =0.) \
        -> Tuple[npt.NDArray[np.float64],Optional[npt.NDArray[np.float64]],Optional[npt.NDArray[np.float64]]]:
    """
    This an empty function that can be used as a basic template for
    implementing the other functions to estimate the gravitational acceleration.
    Every function of this kind needs to have two input parameters:

        - particles, that is an instance of the class :class:`~fireworks.particles.Particles`
        - softening, it is the gravitational softening. The parameters need to be included even
          if the function is not using it. Use a default value of 0.

    The function needs to return a tuple containing three elements:

        - acc, the acceleration estimated for each particle, it needs to be a Nx3 numpy array,
            this element is mandatory it cannot be 0.
        - jerk, time derivative of the acceleration, it is an optional value, if the method
            does not estimate it, just set this element to None. If it is not None, it has
            to be a Nx3 numpy array.
        - pot, gravitational potential at the position of each particle. it is an optional value, if the method
            does not estimate it, just set this element to None. If it is not None, it has
            to be a Nx1 numpy array.

    :param particles: An instance of the class Particles
    :param softening: Softening parameter
    :return: A tuple with 3 elements:

        - acc, Nx3 numpy array storing the acceleration for each particle
        - jerk, Nx3 numpy array storing the time derivative of the acceleration, can be set to None
        - pot, Nx1 numpy array storing the potential at each particle position, can be set to None
    """

    acc = np.zeros(len(particles))
    jerk = None
    pot = None

    return (acc,jerk,pot)

def acceleration_pyfalcon(particles: Particles, softening: float =0.) \
        -> Tuple[npt.NDArray[np.float64],Optional[npt.NDArray[np.float64]],Optional[npt.NDArray[np.float64]]]:
    """
    Estimate the acceleration following the fast-multipole gravity Dehnen2002 solver (https://arxiv.org/pdf/astro-ph/0202512.pdf)
    as implementd in pyfalcon (https://github.com/GalacticDynamics-Oxford/pyfalcon)

    :param particles: An instance of the class Particles
    :param softening: Softening parameter
    :return: A tuple with 3 elements:

        - Acceleration: a NX3 numpy array containing the acceleration for each particle
        - Jerk: None, the jerk is not estimated
        - Pot: a Nx1 numpy array containing the gravitational potential at each particle position
    """

    if not pyfalcon_load: return ImportError("Pyfalcon is not available")

    acc, pot = pyfalcon.gravity(particles.pos,particles.mass,softening,kernel=0)
    jerk = None

    return acc, jerk, pot

def acceleration_direct(particles: Particles, softening: float =0.) \
        -> Tuple[npt.NDArray[np.float64],Optional[npt.NDArray[np.float64]],Optional[npt.NDArray[np.float64]]]:
    """
    Estimate the acceleration following the direct-force method

    :param particles: An instance of the class Particles
    :param softening: Softening parameter
    :return: A tuple with 3 elements:

        - Acceleration: a NX3 numpy array containing the acceleration for each particle
        - Jerk: None, the jerk is not estimated
        - Pot: None, the potential is not estimated
    """
    
    N = len(particles.mass)
    x = particles.pos
    m = particles.mass
    
    acc = np.zeros((N,3),float)
    for i in range(N):
        for j in range(N):
            if(i != j):
                rij = np.linalg.norm(x[i,:]-x[j,:])
                acc[i,:] -= m[j]*(x[i,:]-x[j,:])/(rij**2.+softening**2.)**1.5       # included softening as in Plummer softening. By default set to 0

    jerk = None
    pot = None

    return acc, jerk, pot

def acceleration_direct_vectorised(particles: Particles, softening: float =0.) \
        -> Tuple[npt.NDArray[np.float64],Optional[npt.NDArray[np.float64]],Optional[npt.NDArray[np.float64]]]:
    """
    Estimate the acceleration following the direct-force method fully vectorised

    :param particles: An instance of the class Particles
    :param softening: Softening parameter
    :return: A tuple with 3 elements:

        - Acceleration: a NX3 numpy array containing the acceleration for each particle
        - Jerk: None, the jerk is not estimated
        - Pot: None, the potential is not estimated

    """

    x = particles.pos
    mass = particles.mass
    
    dist = x.T[:,None,:]-x.T[:,:,None]                                     # NxNx3 array containing the vectors xi-xj for all possible particle pairs (note that with this definition dist_ij=xj-xi)
    norm2 = np.sum(dist**2.,axis=0)                                        # NxN array containing the distances squared between all possible particle pairs
    norm3 = (norm2+np.diag(np.full(len(norm2),1.))+softening**2.)**1.5     # NxN array containing the distances cubed between all possible particle pairs (note that a non-zero diagonal has been added to norm2, and Plummer softening has been included but it is 0 by default)
    acc = (np.sum(mass*dist/norm3,axis=2)).T                               # Nx3 array containing the acceleration components of all particles (note that the non-zero diagonal in norm3 avoids NaN outputs)
    
    jerk = None
    pot = None

    return acc, jerk, pot

def acceleration_jerk_direct(particles: Particles, softening: float =0.) \
        -> Tuple[npt.NDArray[np.float64],Optional[npt.NDArray[np.float64]],Optional[npt.NDArray[np.float64]]]:
    """
    Estimate the acceleration and the jerk following the direct-force method

    :param particles: An instance of the class Particles
    :param softening: Softening parameter
    :return: A tuple with 3 elements:

        - Acceleration: a NX3 numpy array containing the acceleration for each particle
        - Jerk: a Nx3 numpy array storing the time derivative of the acceleration
        - Pot: None, the potential is not estimated
    """

    N = len(particles.mass)
    x = particles.pos
    v = particles.vel
    m = particles.mass

    acc = np.zeros((N,3),float)
    jerk = np.zeros([N,3],float)
    for i in range(N):
        for j in range(N):
            if(i != j):
                rij = np.linalg.norm(x[i,:]-x[j,:])
                acc[i,:] -= m[j]*(x[i,:]-x[j,:])/(rij**2.+softening**2.)**1.5     # included softening as in Plummer softening. By default set to 0
                jerk[i,:] -= m[j]*((v[i,:]-v[j,:])/(rij**2.+softening**2.)**1.5-3.*np.dot(x[i,:]-x[j,:],v[i,:]-v[j,:])*(x[i,:]-x[j,:])/(rij**2.+softening**2.)**2.5)     # included softening as in Plummer softening. By default set to 0

    pot = None

    return acc, jerk, pot

def acceleration_cr3bp(particles: Particles, softening: float =0.) \
        -> Tuple[npt.NDArray[np.float64],Optional[npt.NDArray[np.float64]],Optional[npt.NDArray[np.float64]]]:
    """
    Estimate the acceleration of a satellite body in the synodic frame of a Circular Restricted 3 Body Problem

    :param particles: An instance of the class Particles containing the dynamical state of the satellite body 
        and the mass of the smaller body of the binary in the synodic frame
    :param softening: Softening parameter
    :return: A tuple with 3 elements:

        - Acceleration: a 1x3 numpy array containing the acceleration of the satellite body
        - Jerk: None, the jerk is not estimated
        - Pot: None, the potential is not estimated
    """

    x = particles.pos[0]
    v = particles.vel[0]
    mu = particles.mass[0]
    
    r1 = np.sqrt((x[0]+mu)**2.+x[1]**2.+x[2]**2.)
    r2 = np.sqrt((x[0]-1.+mu)**2.+x[1]**2.+x[2]**2.)
    ax = 2.*v[1]+x[0]-(1.-mu)*(x[0]+mu)/r1**3.-mu*(x[0]-1.+mu)/r2**3.
    ay = -2.*v[0]+x[1]-(1.-mu)*x[1]/r1**3.-mu*x[1]/r2**3.
    az = -(1.-mu)*x[2]/r1**3.-mu*x[2]/r2**3.
    acc = np.array([[ax,ay,az]])

    jerk = None
    pot = None

    return acc, jerk, pot

