"""
=========================================================
ODE integrators  (:mod:`fireworks.nbodylib.integrators`)
=========================================================

This module contains a collection of integrators to integrate one step of the ODE N-body problem
The functions included in this module should follow the input/output structure
of the template method :func:`~integrator_template`.

All the functions need to have the following input parameters:

    - particles, an instance of the  class :class:`~fireworks.particles.Particles`
    - tstep, timestep to be used to advance the Nbody system using the integrator
    - acceleration_estimator, it needs to be a function from the module (:mod:`fireworks.nbodylib.dynamics`)
        following the input/output style of the template function
        (:func:`fireworks.nbodylib.dynamics.acceleration_estimate_template`)
    - softening, softening values used to estimate the acceleration
    - external_accelerations, this is an optional input, if not None, it has to be a list
        of additional callable to estimate additional acceleration terms (e.g. an external potential or
        some drag term depending on the particles velocity). Notice that if the integrator uses the jerk
        all these additional terms should return the jerk otherwise the jerk estimate is biased.

Then, all the functions need to return the a tuple with 5 elements:

    - particles, an instance of the class :class:`~fireworks.particles.Particles` containing the
        updated Nbody properties after the integration timestep
    - tstep, the effective timestep evolved in the simulation (for some integrator this can be
        different wrt the input tstep)
    - acc, the final acceleration estimated for each particle, it needs to be a Nx3 numpy array,
        can be set to None
    - jerk, total time derivative of the final acceleration, it is an optional value, if the method
        does not estimate it, just set this element to None. If it is not None, it has
        to be a Nx3 numpy array.
    - pot, total gravitational potential at the final position of each particle, it is an optional value, if the method
        does not estimate it, just set this element to None. If it is not None, it has
        to be a Nx1 numpy array.

"""
from typing import Optional, Tuple, Callable, Union, List
import numpy as np
import numpy.typing as npt
from ..particles import Particles
try:
    import tsunami
    tsunami_load=True
except:
    tsunami_load=False

def integrator_template(particles: Particles,
                        tstep: float,
                        acceleration_estimator: Union[Callable,List],
                        softening: float = 0.,
                        external_accelerations: Optional[List] = None):
    """
    This is an example template of the function you have to implement for the N-body integrators.
    :param particles: Instance of the class :class:`~fireworks.particles.Particles`
    :param tstep: Time-step for current integration (notice some methods can use smaller sub-time step to
    achieve the final result)
    :param acceleration_estimator: It needs to be a function from the module (:mod:`fireworks.nbodylib.dynamics`)
    following the input/output style of the template function (:func:`fireworks.nbodylib.dynamics.acceleration_estimate_template`)
    :param softening: softening parameter for the acceleration estimate, can use 0 as default value
    :param external_accelerations: a list of additional force estimators (e.g. an external potential field) to
    consider to estimate the total acceleration (and if available jerk) on the particles
    :return: A tuple with 5 elements:

        - The updated particles instance
        - tstep, the effective timestep evolved in the simulation (for some integrator this can be
            different wrt the input tstep)
        - acc, Nx3 numpy array storing the final acceleration for each particle, can be set to None
        - jerk, Nx3 numpy array storing the time derivative of the acceleration, can be set to None
        - pot, Nx1 numpy array storing the potential at each particle position, can be set to None

    """

    acc,jerk,potential=acceleration_estimator(particles,softening)

    # Check additional accelerations
    if external_accelerations is not None:
        for ext_acc_estimator in external_accelerations:
            acct,jerkt,potentialt=ext_acc_estimator(particles,softening)
            acc+=acct
            if jerk is not None and jerkt is not None: jerk+=jerkt
            if potential is not None and potentialt is not None: potential+=potentialt

    # Example of an Euler estimate
    particles.pos = particles.pos + particles.vel*tstep # Update posision
    particles.vel = particles.vel + acc*tstep # Update velocity
    particles.set_acc(acc) # Set acceleration

    # Now return the updated particles, the acceleration, jerk (can be None) and potential (can be None)

    return (particles, tstep, acc, jerk, potential)

def integrator_tsunami(particles: Particles,
                       tstep: float,
                       acceleration_estimator: Optional[Callable] = None,
                       softening: float = 0.,
                       external_accelerations: Optional[List] = None):
    """
    Special integrator that is actually a wrapper of the TSUNAMI integrator.
    TSUNAMI is regularised and it has its own way to estimate the acceleration,
    set the timestep and update the system.
    Therefore in this case tstep should not be the timestep of the integration, but rather
    the final time of our simulations, or an intermediate time in which we want to store
    the properties or monitor the sytems.

    .. note::
        In general the TSUNAMI integrator is much faster than any integrator which we can implement
        in this module.
        However, Before to start the proper integration, this function needs to perform some preliminary
        steps to initialise the TSUNAMI integrator. This can add a overhead to the function call.
        Therefore, do not use this integrator with too small timestep. Acutally, the best timstep is the
        one that brings the system directly to the final time. However, if you want to save intermediate steps
        you can split the integration time windows in N sub-parts, calling N times this function.

    .. warning::
        It is important to notice that given the nature of the integrator (based on chain regularisation)
        the final time won't be exactly the one put in input. Take this in mind when using this integrator.
        Notice also that the TSUNAMI integrator will rescale your system to the centre of mass frame of reference.

    .. warning::
        Considering the way the TSUNAMI python wrapper is implemented, the particle positions and velocities are
        updated in place. So if you store the particle.pos or particle.vel inside your loop in a list, each time
        the integrator is called all the elements in the list are updated. Therefore, you will end with a list of pos
        and vel that are equal to the positions and velocities updated in the last tsunami call.
        To avoid this issue, save a copy of the arrays in the list. For example:

    >>> tstart=0
    >>> tintermediate=[5,10,15]
    >>> tcurrent=0
    >>> pos_list=[]
    >>> vel_list=[]
    >>> for t in tintermediate:
    >>>     tstep=t-tcurrent
    >>>     ## NOTICE: Sometime the efftime can be so large that the current time is now larger than the current t
    >>>     ## the simple check below allow to skip these steps and go directly to the t in tintermediate for which
    >>>     ## t>tcurrent and we have to actually integrate the system
    >>>     if tstep<=0.: continue # continue means go to the next step (i.e. next t in the array)
    >>>
    >>>     particles, efftime,_,_,_=integrator_tsunami(particles,tstep)
    >>>
    >>>     # Save the particles positions and velocities
    >>>     pos_list.append(particles.pos.copy())
    >>>     vel_list.append(particles.vel.copy())
    >>>
    >>>     # Here we can save stuff, plot stuff, etc.
    >>>     tcurrent=tcurrent+efftime

    :param particles: Instance of the class :class:`~fireworks.particles.Particles`
    :param tstep: final time of the current integration
    :param acceleration_estimator: Not used
    :param softening: Not used
    :param external_accelerations: Not used
    :return: A tuple with 5 elements:

        - The updated particles instance
        - tstep, the effective timestep evolved in the simulation, it won't be exactly the one in input
        - acc, it is None
        - jerk, it is None
        - pot, it is None

    """

    if not tsunami_load: raise ImportError("Tsunami is not available")

    code = tsunami.Tsunami(1.0, 1.0)
    code.Conf.dcoll = 0.0 # Disable collisions
    # Disable extra forces (already disabled by default)
    code.Conf.wPNs = False  # PNs
    code.Conf.wEqTides = False  # Equilibrium tides
    code.Conf.wDynTides = False  # Dynamical tides

    r=np.ones_like(particles.mass)
    st=np.array(np.ones(len(particles.mass))*(-1), dtype=int)
    code.add_particle_set(particles.pos, particles.vel, particles.mass, r, st)
    # Synchronize internal code coordinates with the Python interface
    code.sync_internal_state(particles.pos, particles.vel)
    # Evolve system from 0 to tstep - NOTE: the system final time won't be exacly tstep, but close
    code.evolve_system(tstep)
    # Synchronize realt to the real internal system time
    time = code.time
    # Synchronize coordinates to Python interface
    code.sync_internal_state(particles.pos, particles.vel)

    return (particles, time, None, None, None)

def integrator_euler(particles: Particles,
                     tstep: float,
                     acceleration_estimator: Union[Callable,List],
                     softening: float = 0.,
                     external_accelerations: Optional[List] = None):
    """
    This integrator is a simple Euler integrator.
    It is not very accurate (since it is first order), but it is very fast.
    It is a good integrator to use for testing purposes.

    :param particles: Instance of the class :class:`~fireworks.particles.Particles`
    :param tstep: Time-step for current integration
    :param acceleration_estimator: It needs to be a function from the module (:mod:`fireworks.nbodylib.dynamics`)
    following the input/output style of the template function (:func:`fireworks.nbodylib.dynamics.acceleration_estimate_template`)
    :param softening: softening parameter for the acceleration estimate, it is 0 as default value
    :param external_accelerations: a list of additional force estimators (e.g. an external potential field) to
    consider to estimate the total acceleration (and if available jerk) on the particles. Set to None by default
    :return: A tuple with 5 elements:

        - The updated particles instance
        - tstep, the effective timestep evolved in the simulation
        - acc, Nx3 numpy array storing the final acceleration for each particle
        - jerk, Nx3 numpy array storing the time derivative of the acceleration
        - pot, Nx1 numpy array storing the potential at each particle position

    """  

    # Initial acceleration (checking for pre-estimated acceleration)
    if particles.acc is not None: acc = particles.acc
    else:
        acc,_,_ = acceleration_estimator(particles,softening)
        # Check additional accelerations
        if external_accelerations is not None:
            for ext_acc_estimator in external_accelerations:
                acct,_,_ = ext_acc_estimator(particles,softening)
                acc += acct
    
    # Euler integration
    particles.pos = particles.pos + particles.vel*tstep     # Update position
    particles.vel = particles.vel + acc*tstep               # Update velocity

    # Final acceleration, jerk and potential
    acc,jerk,pot = acceleration_estimator(particles,softening)
    if external_accelerations is not None:
        for ext_acc_estimator in external_accelerations:
            acct,jerkt,potentialt = ext_acc_estimator(particles,softening)
            acc += acct
            if jerk is not None and jerkt is not None: jerk += jerkt
            else: jerk = None        # If at least one of the jerks is None, we cannot estimate the total jerk
            if pot is not None and potentialt is not None: pot += potentialt
            else: pot = None         # If at least one of the potentials is None, we cannot estimate the total potential

    # Set final acceleration
    particles.set_acc(acc)

    # Now return the updated particles, the acceleration, jerk (can be None) and potential (can be None)
    return (particles, tstep, acc, jerk, pot)

def integrator_midpoint(particles: Particles,
                       tstep: float,
                       acceleration_estimator: Union[Callable,List],
                       softening: float = 0.,
                       external_accelerations: Optional[List] = None):
    """
    This integrator is a Midpoint integrator (Runge-Kutta of 2nd order). It is second order,
    thus it is more accurate than the Euler scheme. It requires just 2 force evaluations.
    However it is not time-reversible, nor symplectic: it isn't a stable integrator.
    It is good for testing purposes.

    :param particles: Instance of the class :class:`~fireworks.particles.Particles`
    :param tstep: Time-step for current integration
    :param acceleration_estimator: It needs to be a function from the module (:mod:`fireworks.nbodylib.dynamics`)
    following the input/output style of the template function (:func:`fireworks.nbodylib.dynamics.acceleration_estimate_template`)
    :param softening: softening parameter for the acceleration estimate, it is 0 as default value
    :param external_accelerations: a list of additional force estimators (e.g. an external potential field) to
    consider to estimate the total acceleration (and if available jerk) on the particles. Set to None by default
    :return: A tuple with 5 elements:

        - The updated particles instance
        - tstep, the effective timestep evolved in the simulation
        - acc, Nx3 numpy array storing the final acceleration for each particle
        - jerk, Nx3 numpy array storing the time derivative of the acceleration
        - pot, Nx1 numpy array storing the potential at each particle position

    """
    
    # Storing a copy of initial position and velocity
    old_pos = np.copy(particles.pos)
    old_vel = np.copy(particles.vel)

    # Midpoint integration
    k1_pos = tstep * old_vel
    if particles.acc is not None: acc = particles.acc
    else:
        acc,_,_ = acceleration_estimator(particles,softening)
        if external_accelerations is not None:
            for ext_acc_estimator in external_accelerations:
                acct,_,_ = ext_acc_estimator(particles,softening)
                acc += acct
    k1_vel = tstep * acc
    k2_pos = tstep * (old_vel+0.5*k1_vel)
    particles.pos = old_pos + 0.5*k1_pos
    particles.vel = old_vel + 0.5*k1_vel
    acc,_,_ = acceleration_estimator(particles,softening)
    if external_accelerations is not None:
        for ext_acc_estimator in external_accelerations:
            acct,_,_ = ext_acc_estimator(particles,softening)
            acc += acct
    k2_vel = tstep * acc
    particles.pos = old_pos + k2_pos
    particles.vel = old_vel + k2_vel

    # Final acceleration, jerk and potential
    acc,jerk,pot = acceleration_estimator(particles,softening)
    if external_accelerations is not None:
        for ext_acc_estimator in external_accelerations:
            acct,jerkt,potentialt = ext_acc_estimator(particles,softening)
            acc += acct
            if jerk is not None and jerkt is not None: jerk += jerkt
            else: jerk = None        # If at least one of the jerks is None, we cannot estimate the total jerk
            if pot is not None and potentialt is not None: pot += potentialt
            else: pot = None         # If at least one of the potentials is None, we cannot estimate the total potential

    # Set final acceleration
    particles.set_acc(acc)

    # Now return the updated particles, the acceleration, jerk (can be None) and potential (can be None)
    return (particles, tstep, acc, jerk, pot)

def integrator_leapfrog(particles: Particles,
                        tstep: float,
                        acceleration_estimator: Union[Callable,List],
                        softening: float = 0.,
                        external_accelerations: Optional[List] = None):
    """
    This integrator is a Leapfrog integrator (sometimes referred as Velocity Verlet).
    It is more accurate than the Euler integrator, since it is second order. It is time-reversible.
    It is a good integrator to use for various purposes over long timescales,
    especially since it is symplectic (i.e., it conserves the total energy of the system):
    it is very stable.

    :param particles: Instance of the class :class:`~fireworks.particles.Particles`
    :param tstep: Time-step for current integration
    :param acceleration_estimator: It needs to be a function from the module (:mod:`fireworks.nbodylib.dynamics`)
    following the input/output style of the template function (:func:`fireworks.nbodylib.dynamics.acceleration_estimate_template`)
    :param softening: softening parameter for the acceleration estimate, it is 0 as default value
    :param external_accelerations: a list of additional force estimators (e.g. an external potential field) to
    consider to estimate the total acceleration (and if available jerk) on the particles. Set to None by default
    :return: A tuple with 5 elements:

        - The updated particles instance
        - tstep, the effective timestep evolved in the simulation
        - acc, Nx3 numpy array storing the final acceleration for each particle
        - jerk, Nx3 numpy array storing the time derivative of the acceleration
        - pot, Nx1 numpy array storing the potential at each particle position

    """

    # Initial acceleration (checking for pre-estimated acceleration)
    if particles.acc is not None: acc = particles.acc
    else:
        acc,_,_ = acceleration_estimator(particles,softening)
        # Check additional accelerations
        if external_accelerations is not None:
            for ext_acc_estimator in external_accelerations:
                acct,_,_ = ext_acc_estimator(particles,softening)
                acc += acct

    # Leapfrog integration
    particles.vel = particles.vel + 0.5*acc*tstep             # Kick of velocity to midpoint of time-step
    particles.pos = particles.pos + particles.vel*tstep       # Drift of position to end of time-step
    acc,_,_ = acceleration_estimator(particles,softening)     # Acceleration estimate at end of time-step 
    if external_accelerations is not None:                    # Contribution due to eventual external accelerations
        for ext_acc_estimator in external_accelerations:
            acct,_,_ = ext_acc_estimator(particles,softening) # Note that if ext_acc=f(x,v) then ext_acc(t+h)=f(x(t+h),v(t+h/2))
            acc += acct
    particles.vel = particles.vel + 0.5*acc*tstep             # Kick of velocity to end of time-step

    # Final acceleration, jerk and potential
    acc,jerk,pot = acceleration_estimator(particles,softening)
    if external_accelerations is not None:
        for ext_acc_estimator in external_accelerations:
            acct,jerkt,potentialt = ext_acc_estimator(particles,softening)
            acc += acct
            if jerk is not None and jerkt is not None: jerk += jerkt
            else: jerk = None        # If at least one of the jerks is None, we cannot estimate the total jerk
            if pot is not None and potentialt is not None: pot += potentialt
            else: pot = None         # If at least one of the potentials is None, we cannot estimate the total potential

    # Set final acceleration
    particles.set_acc(acc)
    
    # Now return the updated particles, the acceleration, jerk (can be None) and potential (can be None)
    return (particles, tstep, acc, jerk, pot)

def integrator_rk4(particles: Particles,
                       tstep: float,
                       acceleration_estimator: Union[Callable,List],
                       softening: float = 0.,
                       external_accelerations: Optional[List] = None):
    """
    This integrator is a (4th order) Runge-Kutta integrator. It is fourth order, thus it is very accurate.
    However it is not time-reversible, nor symplectic, and it requires 4 acceleration evaluations.
    It is one of best integrators considering the trade-off between accuracy and number of force evaluations,
    indeed it is usually one of the preferred method for orbit integration.

    :param particles: Instance of the class :class:`~fireworks.particles.Particles`
    :param tstep: Time-step for current integration
    :param acceleration_estimator: It needs to be a function from the module (:mod:`fireworks.nbodylib.dynamics`)
    following the input/output style of the template function (:func:`fireworks.nbodylib.dynamics.acceleration_estimate_template`)
    :param softening: softening parameter for the acceleration estimate, it is 0 as default value
    :param external_accelerations: a list of additional force estimators (e.g. an external potential field) to
    consider to estimate the total acceleration (and if available jerk) on the particles. Set to None by default
    :return: A tuple with 5 elements:

        - The updated particles instance
        - tstep, the effective timestep evolved in the simulation
        - acc, Nx3 numpy array storing the final acceleration for each particle
        - jerk, Nx3 numpy array storing the time derivative of the acceleration
        - pot, Nx1 numpy array storing the potential at each particle position

    """
   
    # Storing a copy of initial position and velocity
    old_pos = np.copy(particles.pos)
    old_vel = np.copy(particles.vel)

    # Runge-Kutta 4th order integration
    k1_pos = tstep * old_vel
    if particles.acc is not None: acc = particles.acc
    else:
        acc,_,_ = acceleration_estimator(particles,softening)
        if external_accelerations is not None:
            for ext_acc_estimator in external_accelerations:
                acct,_,_ = ext_acc_estimator(particles,softening)
                acc += acct
    k1_vel = tstep * acc
    k2_pos = tstep * (old_vel+0.5*k1_vel)
    particles.pos = old_pos + 0.5*k1_pos
    particles.vel = old_vel + 0.5*k1_vel
    acc,_,_ = acceleration_estimator(particles,softening)
    if external_accelerations is not None:
        for ext_acc_estimator in external_accelerations:
            acct,_,_ = ext_acc_estimator(particles,softening)
            acc += acct
    k2_vel = tstep * acc
    k3_pos = tstep * (old_vel+0.5*k2_vel)
    particles.pos = old_pos + 0.5*k2_pos
    particles.vel = old_vel + 0.5*k2_vel
    acc,_,_ = acceleration_estimator(particles,softening)
    if external_accelerations is not None:
        for ext_acc_estimator in external_accelerations:
            acct,_,_ = ext_acc_estimator(particles,softening)
            acc += acct
    k3_vel = tstep * acc
    k4_pos = tstep * (old_vel+k3_vel)
    particles.pos = old_pos + k3_pos
    particles.vel = old_vel + k3_vel
    acc,_,_ = acceleration_estimator(particles,softening)
    if external_accelerations is not None:
        for ext_acc_estimator in external_accelerations:
            acct,_,_ = ext_acc_estimator(particles,softening)
            acc += acct
    k4_vel = tstep * acc
    particles.pos = old_pos + (k1_pos+2.*k2_pos+2.*k3_pos+k4_pos)/6.
    particles.vel = old_vel + (k1_vel+2.*k2_vel+2.*k3_vel+k4_vel)/6.

    # Final acceleration, jerk and potential
    acc,jerk,pot = acceleration_estimator(particles,softening)
    if external_accelerations is not None:
        for ext_acc_estimator in external_accelerations:
            acct,jerkt,potentialt = ext_acc_estimator(particles,softening)
            acc += acct
            if jerk is not None and jerkt is not None: jerk += jerkt
            else: jerk = None        # If at least one of the jerks is None, we cannot estimate the total jerk
            if pot is not None and potentialt is not None: pot += potentialt
            else: pot = None         # If at least one of the potentials is None, we cannot estimate the total potential

    # Set final acceleration
    particles.set_acc(acc)

    # Now return the updated particles, the acceleration, jerk (can be None) and potential (can be None)
    return (particles, tstep, acc, jerk, pot)

def integrator_hermite(particles: Particles,
                       tstep: float,
                       acceleration_estimator: Union[Callable,List],
                       softening: float = 0.,
                       external_accelerations: Optional[List] = None):
    """
    This integrator is a Hermite integrator. It is fourth order, thus it is very accurate.
    It is more efficient than a Runge-Kutta of the same order since it requires less force evaluations (only 2).
    Indeed it is preferred to solve collisional N-body problems, as the acceleration estimate is the bottleneck of the simulation.
    However it is not time-reversible and not symplectic: total energy is not conserved,
    so it loses accuracy after a certain characteristic timescale.
    A good feature is the possibility to use the difference between predictions and corrections to set adaptive timesteps.
    NOTE: it requires jerk estimate, which is not always available for all acceleration estimator.
    
    :param particles: Instance of the class :class:`~fireworks.particles.Particles`
    :param tstep: Time-step for current integration
    :param acceleration_estimator: It needs to be a function from the module (:mod:`fireworks.nbodylib.dynamics`)
    following the input/output style of the template function (:func:`fireworks.nbodylib.dynamics.acceleration_estimate_template`)
    :param softening: softening parameter for the acceleration estimate, it is 0 as default value
    :param external_accelerations: a list of additional force estimators (e.g. an external potential field) to
    consider to estimate the total acceleration (and if available jerk) on the particles. Set to None by default
    :return: A tuple with 5 elements:

        - The updated particles instance
        - tstep, the effective timestep evolved in the simulation
        - acc, Nx3 numpy array storing the final acceleration for each particle
        - jerk, Nx3 numpy array storing the time derivative of the acceleration
        - pot, Nx1 numpy array storing the potential at each particle position

    """

    # Initial acceleration and jerk (checking for pre-estimated acceleration and jerk)
    if particles.acc is not None and particles.jerk is not None:
        acc = particles.acc
        jerk = particles.jerk
    else:
        acc,jerk,_ = acceleration_estimator(particles,softening)
        # Jerk is required for Hermite, so if None return error
        if jerk is None: raise NotImplementedError("Estimator doesn't return required jerk, please change estimator or integrator")
        # Check additional accelerations
        if external_accelerations is not None:
            for ext_acc_estimator in external_accelerations:
                acct,jerkt,_ = ext_acc_estimator(particles,softening)
                acc += acct
                if jerkt is None: raise NotImplementedError("External estimator doesn't return required jerk, please change estimator or integrator")
                jerk += jerkt           # This is executed only if both jerk and jerkt are not None

    # Storing a copy of initial position and velocity
    old_pos = np.copy(particles.pos)
    old_vel = np.copy(particles.vel)

    # Predictor sub-step
    particles.pos = old_pos + tstep*old_vel + tstep**2.*acc/2. + tstep**3.*jerk/6.
    particles.vel = old_vel + tstep*acc + tstep**2.*jerk/2.

    # Predicted acceleration and jerk evaluation
    accp,jerkp,_ = acceleration_estimator(particles,softening)
    if external_accelerations is not None:
        for ext_acc_estimator in external_accelerations:
            acct,jerkt,_ = ext_acc_estimator(particles,softening)
            accp += acct
            jerkp += jerkt

    # Corrector sub-step
    particles.vel = old_vel + tstep*(acc+accp)/2. + tstep**2.*(jerk-jerkp)/12.
    particles.pos = old_pos + tstep*(old_vel+particles.vel)/2. + tstep**2.*(acc-accp)/12.

    # Final acceleration, jerk and potential
    acc,jerk,pot = acceleration_estimator(particles,softening)
    if external_accelerations is not None:
        for ext_acc_estimator in external_accelerations:
            acct,jerkt,potentialt = ext_acc_estimator(particles,softening)
            acc += acct
            jerk += jerkt      
            if pot is not None and potentialt is not None: pot += potentialt
            else: pot = None         # If at least one of the potentials is None, we cannot estimate the total potential

    # Set final acceleration and jerk
    particles.set_acc(acc)
    particles.set_jerk(jerk)

    # Now return the updated particles, the acceleration, jerk (can be None) and potential (can be None)
    return (particles, tstep, acc, jerk, pot)
