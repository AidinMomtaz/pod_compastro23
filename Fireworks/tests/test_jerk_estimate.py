import pytest
import numpy as np
import fireworks.nbodylib.dynamics as fdyn
from fireworks.particles import Particles



def test_jerk_2body():
    """
    Simple two body case of two bodies along the x axis (at x=0 and x=1) of mass 1 at a distance 1,
    with velocties along the y axis equal to 1 and -1 respectively,
    therefore the jerk on the first body is (0,-2,0) and on the secondy body (0,2,0).

    """
    
    facc_list = [fdyn.acceleration_jerk_direct]   # at the moment only acceleration_jerk_direct can estimate jerk

    pos = np.array([[0.,0.,0.],[1.,0.,0.]])
    vel = np.array([[0.,1.,0.],[0.,-1.,0.]])
    mass = np.ones(len(pos))

    part = Particles(pos,vel,mass)

    true_jerk = np.array([[0.,-2.,0.],[0.,2.,0.]])

    for facc in facc_list:
        _,jerk,_=facc(part)

        dx = np.abs(jerk-true_jerk)

        assert np.all(dx<=1e-11)

def test_acceleration_row():
    """
    Another simple case in which we have N bodies along the x axis,
    each body is at a distance r from the origin (the first has r=0),
    the mass of each body is equal to r^3,
    the velocity of the body at the center r=0 is (0,0,0),
    the velocity of all the other bodies is (0,1,0).
    Therefore the y-component of the jerk on the body at the center r=0 is equal to the number of the other bodies.

    """

    facc_list = [fdyn.acceleration_jerk_direct]   # at the moment only acceleration_jerk_direct can estimate jerk

    x = np.array(np.arange(11),dtype=float)
    y = np.zeros_like(x)
    z = np.zeros_like(x)

    pos = np.vstack([x,y,z]).T
    vel = np.zeros_like(pos)
    vel[:,1] = 1.
    vel[0,1] = 0.
    mass = x*x*x
    mass[0] = 1
    part = Particles(pos,vel,mass)

    jerk_true_0 = len(mass)-1

    for facc in facc_list:
        _,jerk,_=facc(part)
        dx = jerk[0,1]-jerk_true_0

        assert pytest.approx(dx, 1e-10) == 0.
        # pytest approx is used to introduce a tollerance in the comparison (in this case 1e-10)


