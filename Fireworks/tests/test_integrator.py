import pytest
import numpy as np
import fireworks.nbodylib.dynamics as fdyn
from fireworks.particles import Particles
from fireworks.ic import ic_random_normal
import fireworks.nbodylib.integrators as fint



def simple_test(integrator):
    """
    Simple test template to test an integrator
    The idea is very simple, assume an acceleration function that returns always 0
    therefore the after an integration step the velocity remains the same
    and the position is pos+vel*dt where dt is the effective integration timestep

    """

    N = 10
    par = ic_random_normal(N,mass=1)
    tstep = 1

    par_old = par.copy() # Copy the initial parameter
    print(par.vel[0])
    print(par.pos[0])

    # Just return 2 Nx3 filled with 0 (acceleration and jerk) + 1 None (potential)
    acc_fake = lambda particles, softening: (np.zeros_like(particles.pos), np.zeros_like(particles.pos), None)
  
    par,teffective,_,_,_=integrator(particles=par,
                                               tstep=tstep,
                                               acceleration_estimator=acc_fake,
                                               softening=0.)

    pos_test = par_old.pos + par_old.vel*teffective

    # Test velocity, we expect no change in velocity (we use a very small number instead of 0
    # in order to take into account possible round-off errors)
    assert np.all( np.abs(par.vel - par_old.vel) <= 1e-10)

    # Test position
    assert np.all( np.abs(par.pos - pos_test) <= 1e-10)

def test_integrator_template():
    """
    Apply the simple_test to the integrator_template

    """
    
    simple_test(fint.integrator_template)

def test_integrator_euler():
    """
    Apply the simple_test to the integrator_euler

    """
    
    simple_test(fint.integrator_euler)

def test_integrator_midpoint():
    """
    Apply the simple_test to the integrator_midpoint

    """
    
    simple_test(fint.integrator_midpoint)

def test_integrator_leapfrog():
    """
    Apply the simple_test to the integrator_leapfrog

    """
    
    simple_test(fint.integrator_leapfrog)

def test_integrator_rk4():
    """
    Apply the simple_test to the integrator_rk4

    """
    
    simple_test(fint.integrator_rk4)

def test_integrator_hermite():
    """
    Apply the simple_test to the integrator_hermite

    """
    
    simple_test(fint.integrator_hermite)