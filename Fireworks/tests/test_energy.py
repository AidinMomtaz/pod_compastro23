import pytest
import numpy as np
from fireworks.particles import Particles



def test_energy_3bodies():
    """
    Simple example in which 3 bodies are in position (0,0,0), (1,0,0) and (-1,0,0)
    with mass respectively of 1,2 and 3 and velocities (0,-1,0), (0,1,0) and (-1,0,0).
    Then the total kinetic energy is 3 and the total potential energy is -8.
    The total energy is 3-8=-5.

    """
    
    pos = np.array([[0.,0.,0.],[1.,0.,0.],[-1.,0.,0.]])
    vel = np.array([[0.,-1.,0.],[0.,1.,0.],[-1.,0.,0.]])
    mass = np.array([1.,2.,3.])
    part = Particles(pos,vel,mass)

    true_E = np.array([-5.,3.,-8.])  # [Etot,Ekin,Epot]

    E = np.array(part.Etot())
    # Etot method of Particles returns Etot,Ekin,Epot

    deltaE = np.abs(E-true_E)

    assert np.all(deltaE<=1e-11)
