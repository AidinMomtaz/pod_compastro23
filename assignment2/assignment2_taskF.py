import numpy as np
import fireworks.ic as fic
import fireworks.nbodylib.dynamics as fdyn
from fireworks.particles import Particles
import time
import matplotlib.pyplot as plt

num = np.arange(10,5000,1000,int)    # preparing array of numbers of particles with values in [10,5000) with step 1000
mass_min = 1.
mass_max = 100.
pos_min = -100.
pos_max = 100.
vel_min = 0.
vel_max = 0.
facc_list = [fdyn.acceleration_jerk_direct]    # at the moment only acceleration_jerk_direct can estimate jerk 
tdirect = []
#tlist = []    # template list to collect times for a newly implemented jerk estimator (remember to update facc_list, loop and plot)

for N in num:
    part = fic.ic_random_uniform(N,mass_min,mass_max,pos_min,pos_max,vel_min,vel_max)    # generating initial conditions
    for facc in facc_list:
        t1 = time.perf_counter()
        facc(part)                  # estimating acceleration and jerk for a specific value of N and a specific estimation method
        t2 = time.perf_counter()
        dt = t2-t1
        if(facc==facc_list[0]):
            tdirect.append(dt)      # loading time in direct_method time list
#       if(facc==facc_list[ ]):     # template condition to collect time for a newly implemented jerk estimator (remember to update facc_list, tlist and plot) 
#           tlist.append(dt)      

plt.plot(num,tdirect,label='direct')
#plt.plot(num,tlist,label=' ')      # template plot for a newly implemented jerk estimator (remember to update facc_list, tlist and loop)

plt.xlabel('Number of particles')
plt.ylabel('Estimation time')
plt.legend(loc='upper left')
plt.savefig('ass2-taskF.pdf')
plt.yscale('log')
plt.savefig('ass2-taskF-log.pdf')